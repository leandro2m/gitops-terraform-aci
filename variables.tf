variable "apic_url" {}
variable "apic_username" {}
variable "apic_password" {
}

variable "user" {
  description = "Login information"
  type        = map(any)
  default = {
    username = "admin"
    password = "!v3G@!4@Y"
    url      = "https://sandboxapicdc.cisco.com"
  }
}